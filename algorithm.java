/**
 * Definition for a binary tree node.
 * public class TreeNode {
 *     int val;
 *     TreeNode left;
 *     TreeNode right;
 *     TreeNode() {}
 *     TreeNode(int val) { this.val = val; }
 *     TreeNode(int val, TreeNode left, TreeNode right) {
 *         this.val = val;
 *         this.left = left;
 *         this.right = right;
 *     }
 * }
 */
class Solution {
    //List of all possible leaves
    LinkedList<String> array = new LinkedList();
    public List<String> binaryTreePaths(TreeNode root) {
        rec(root, new LinkedList());
        return array;
    }
     /**
     * function returns all root-to-leaf paths.
     * @param root current node
     * @param way list of passed nodes
     */
    public void rec(TreeNode root, List<TreeNode> wayPrev){
        if (root == null) return;  
        LinkedList<TreeNode> way =new LinkedList(wayPrev);
        if(root.left == null && root.right ==null){
            StringBuilder x= new StringBuilder();
            for(TreeNode node : way){
                x.append(node.val+"->");
            }
            x.append(root.val);
            array.add(x.toString());
            return;
        }
        way.add(root);
        rec(root.left, way);
        rec(root.right, way);
        
    }
}