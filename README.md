# BinaryTreePaths
Given a binary tree, return all root-to-leaf paths.
Link: https://leetcode.com/arquemiso/

### Implementation
I used a recursive function to traverse all possible branches of a binary tree.

### Notations.
Algorithm time complexity: O(n)
Algorithm space complexity: O(n) (we have to create 2N arraylists)

### Result
![](images/rez.png)
